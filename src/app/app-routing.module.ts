import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [

  {
    path:'',
    loadChildren:() => import('./cluster/cluster.module').then(m => m.ClusterModule)
  },

      {
        path:'',
        loadChildren:() => import('./menu/menu.module').then(m => m.MenuModule)
      },
      {
        path:'',
        loadChildren:() => import('./slider/slider.module').then(m => m.SliderModule)
      },
      {
     path:'',
     loadChildren:() => import('./card/card.module').then(m => m.CardModule)

      }




];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
