import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClusterRoutingModule } from './cluster-routing.module';
import { ClusterComponent } from './components/cluster/cluster.component';


import { MenuModule } from "./../menu/menu.module";
import { SliderModule } from '../slider/slider.module';
import { CardModule } from "./../card/card.module";


@NgModule({
  declarations: [
    ClusterComponent,


  ],
  imports: [
    CommonModule,
    ClusterRoutingModule,
    MenuModule,
    SliderModule,
    CardModule
  ],
  exports:[
    ClusterComponent,

  ]
})
export class ClusterModule { }
