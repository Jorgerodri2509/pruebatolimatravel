import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuRoutingModule } from './menu-routing.module';
import { MenuComponent } from './components/menu/menu.component';
import { SliderModule } from './../slider/slider.module';
import { CardModule } from "./../card/card.module";


@NgModule({
  declarations: [
    MenuComponent,


  ],
  imports: [
    CommonModule,
    MenuRoutingModule,
    SliderModule,
    CardModule
  ],
  exports:[
    MenuComponent
  ]
})
export class MenuModule { }
