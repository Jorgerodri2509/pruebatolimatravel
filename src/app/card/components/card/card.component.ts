import { AfterViewInit, Component, Input, OnInit } from '@angular/core';



import swiper from 'swiper/bundle';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit, AfterViewInit {

width : number;
elements:number;


  card = {
    card1: {
      image: 'assets/imagesCard/imageCard1.png',
      link: 'assets/imagesCard/toliAventura.png',

      title: 'Ratfing Aventura',
      text: 'En Coello-Cócora, un lugar salvaje, natural y a 20 minutos de la ciudad de Ibagué́, podrás vivir una experiencia arriesgada e inigualable',
      iconCall:'assets/imagesCard/call.png',
      ibague: 'Ibague',
      days: '1 Dia',
      iconExperience:'assets/imagesCard/añosDeExperiencia.png',

      price: '90.000 COP',
      button: 'Ver más',
    },
    card2: {
      image: 'assets/imagesCard/imageCard2.png',
      link: 'assets/imagesCard/truman.png',

      title: 'Nevado del Tolima',
      text: 'El Nevado del Tolima es un volcán localizado en la Cordillera Central de los Andes de Colombia, en jurisdicción de los municipios tolimenses de Anzoátegui e Ibagué.',
      iconCall:'assets/imagesCard/call.png',
      ibague: 'Ibague',
      iconExperience:'assets/imagesCard/añosDeExperiencia.png',
      days: '1 Dia',
      price: '150.000 COP',
      button: 'Ver más',
    },
    card3: {
      image: 'assets/imagesCard/imageCard3.png',
      link: 'assets/imagesCard/camaleon.png',
      title: 'Cascadas de Payande',
      text: 'Las cascadas de Payandé se caracterizan por su riqueza hídrica y paisaje natural. También, desde hace tiempo se ha convertido en el escenario favorito de los ibaguereños',
      iconCall:'assets/imagesCard/call.png',
      ibague: 'Ibague',
      iconExperience:'assets/imagesCard/añosDeExperiencia.png',

      days: '1 Dia',
      price: '30.000 COP',
      button: 'Ver más',
    },
    card4: {
      image: 'assets/imagesCard/imageCard4.jpg',
      link: 'assets/imagesCard/living.png',
      title: 'Hotel Campestre',
      text: 'Club Campestre Copiclub sinónimo de confortabilidad y entretenimiento, ubicado a 20 minutos del centro de la Ciudad de Ibagué',
      iconCall:'assets/imagesCard/call.png',

      ibague: 'Ibague',
      iconExperience:'assets/imagesCard/añosDeExperiencia.png',

      days: '1 Dia',
      price: '10.000 COP',
      button: 'Ver más',
    },
  };


  data= Object.values(this.card);



  constructor() {

   }

  ngAfterViewInit(): void {
    const mySwiper = new swiper('.swiper', {

      loop: true,
      slidesPerView:3,
      spaceBetween:15,
      autoplay:true,

    });
  }

  ngOnInit(): void {}

}
