import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SliderRoutingModule } from './slider-routing.module';
import { SliderComponent } from './components/slider/slider.component';
import { CardModule } from "./../card/card.module";



@NgModule({
  declarations: [
    SliderComponent
  ],
  imports: [
    CommonModule,
    SliderRoutingModule,
    CardModule
  ],
  exports:[
    SliderComponent
  ]
})
export class SliderModule { }
